﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ExampleASPDotNet
{
    public partial class index : System.Web.UI.Page
    {
        SqlConnection sqlCon = new SqlConnection(@"Data Source=SECRETCODE;Initial Catalog=examplePFP;Persist Security Info=True;User ID=exam;Password=pass");

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetGender();
                FillGridView();
            }
        }

        private void GetGender()
        {
            if (sqlCon.State == ConnectionState.Closed)
                sqlCon.Open();
            SqlDataAdapter sqlDa = new SqlDataAdapter("GenderGetAll", sqlCon);
            sqlDa.SelectCommand.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();
            sqlDa.Fill(dt);
            ddlGender.DataSource = dt;
            ddlGender.DataValueField = "Id";
            ddlGender.DataTextField = "GenderName";
            ddlGender.DataBind();

            sqlCon.Close();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (sqlCon.State == ConnectionState.Closed)
                sqlCon.Open();

            SqlCommand sqlCmd = new SqlCommand("ContactCreateOrUpdate", sqlCon);
            sqlCmd.CommandType = CommandType.StoredProcedure;
            sqlCmd.Parameters.AddWithValue("@EmpId", hidEmpId.Value=="" ? 0 : Convert.ToInt32(hidEmpId.Value));
            sqlCmd.Parameters.AddWithValue("@GenderId", Convert.ToInt32(ddlGender.SelectedValue));
            sqlCmd.Parameters.AddWithValue("@FirstName", txtFirstName.Text);
            sqlCmd.Parameters.AddWithValue("@LastName", txtLastName.Text);
            sqlCmd.Parameters.AddWithValue("@Phone", txtPhone.Text);
            sqlCmd.Parameters.AddWithValue("@Address", txtAddress.Text);
            sqlCmd.ExecuteNonQuery();
            sqlCon.Close();
            string empId = hidEmpId.Value;
            Clear();
            if (empId == "")
                lblSuccessMessage.Text = "Save Successfully.";
            else
                lblSuccessMessage.Text = "Update Successfully.";

            FillGridView();

        }

        private void FillGridView()
        {
            if (sqlCon.State == ConnectionState.Closed)
                sqlCon.Open();
            SqlDataAdapter sqlDa = new SqlDataAdapter("ContactViewAll", sqlCon);
            sqlDa.SelectCommand.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();
            sqlDa.Fill(dt);
            sqlCon.Close();
            gvEmployee.DataSource = dt;
            gvEmployee.DataBind();
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Clear();
        }

        private void Clear()
        {
            hidEmpId.Value = "";
            lblSuccessMessage.Text = lblErrorMessage.Text = "";
            ddlGender.SelectedIndex = -1;
            txtFirstName.Text = txtLastName.Text = txtPhone.Text = txtAddress.Text = "";
            btnSave.Text = "Save";
            btnCancel.Enabled = false;
        }

        protected void lnkView_Click(object sender, EventArgs e)
        {
            int EmpId = Convert.ToInt32((sender as LinkButton).CommandArgument);

            if (sqlCon.State == ConnectionState.Closed)
                sqlCon.Open();

            SqlDataAdapter sqlDa = new SqlDataAdapter("ContactGetById", sqlCon);
            sqlDa.SelectCommand.CommandType = CommandType.StoredProcedure;
            sqlDa.SelectCommand.Parameters.AddWithValue("@EmpId", EmpId);
            DataTable dt = new DataTable();
            sqlDa.Fill(dt);
            sqlCon.Close();

            hidEmpId.Value = EmpId.ToString();
            ddlGender.SelectedValue = dt.Rows[0]["GenderId"].ToString();
            txtFirstName.Text = dt.Rows[0]["FirstName"].ToString();
            txtLastName.Text = dt.Rows[0]["LastName"].ToString();
            txtPhone.Text = dt.Rows[0]["Phone"].ToString();
            txtAddress.Text = dt.Rows[0]["Address"].ToString();

            btnCancel.Enabled = true;
        }

        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            int EmpId = Convert.ToInt32((sender as LinkButton).CommandArgument);

            if (sqlCon.State == ConnectionState.Closed)
                sqlCon.Open();

            SqlCommand sqlCmd = new SqlCommand("ContactDelete", sqlCon);
            sqlCmd.CommandType = CommandType.StoredProcedure;
            sqlCmd.Parameters.AddWithValue("@EmpId", EmpId);
            sqlCmd.ExecuteNonQuery();
            sqlCon.Close();

            Clear();
            FillGridView();
            lblSuccessMessage.Text = "Delete Successfully.";
        }
    }
}